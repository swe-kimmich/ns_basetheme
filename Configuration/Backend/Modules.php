<?php

use NITSAN\NsBasetheme\Controller\NsConstantEditorController;

return [
    'nitsan_module' => [
        'labels' => 'LLL:EXT:ns_basetheme/Resources/Private/Language/BackendModule.xlf',
        'iconIdentifier' => 'module-nsbasetheme',
        'position' => ['after' => 'web'],
    ],
    'nitsan_nsbasethememodule' => [
        'parent' => 'nitsan_module',
        'position' => ['before' => 'top'],
        'access' => 'user,group',
        'path' => '/module/nitsan/NsBasetheme',
        'iconIdentifier' => 'submodule-nsbasetheme',
        'navigationComponent' => '@typo3/backend/page-tree/page-tree-element',
        'labels' => 'LLL:EXT:ns_basetheme/Resources/Private/Language/locallang_basethememodule.xlf',
        'extensionName' => 'NsBasetheme',
        'routes' => [
            '_default' => [
                'target' => NsConstantEditorController::class . '::handleRequest',
            ],
        ],
        'moduleData' => [
            'selectedTemplatePerPage' => [],
            'selectedCategory' => '',
        ],
    ],
];
